const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const rulesForJavascript = {
  test: /\.(js|jsx)$/,
  exclude: /node_modules/,
  use: {
    loader: 'babel-loader',
    options: { // Babel packages necessary to be able to transpillar the JavaScript code
      presets: [
        '@babel/preset-env',
        ['@babel/preset-react', { runtime: 'automatic' }],
        {
          plugins: ['@babel/plugin-transform-runtime']
        }
      ]
    }
  }
}

const rulesForStyles = {
  test: /\.css$/,
  use: [
    'style-loader',
    'css-loader'
  ]
}

const rulesForSasStyles = {
    test: /\.(s(a|c)ss)$/,
    use: ['style-loader', 'css-loader', 'sass-loader'],
}

const rules = [rulesForJavascript, rulesForStyles, rulesForSasStyles]

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.resolve(__dirname, 'build'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  devServer: {
    open: true,
    compress: true,
    port: 3000,
    static: './build',
  },
  devtool: 'source-map',
  module: {
    rules: rules
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ],
  resolve: {
    extensions: ['.jsx', '.ts', '.js']
  }
}
