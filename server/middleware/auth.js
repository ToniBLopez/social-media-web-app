const jwt = require('jsonwebtoken')
const path = require('path')
require("dotenv").config({ path: path.resolve(__dirname, '../../.env') })

module.exports = (req, res, next) => {
  try {
    let token = req.headers.authorization
    if (!token) {
      return res.status(403).send({ error: 'Access Denied' })
    }
    if (token.startsWith("Bearer ")) {
      token = token.slice(7, token.length).trimLeft()
    }
    const decoded = jwt.verify(token, process.env.JWT_SECRET)
    req.user = decoded
    next()
  } catch (err) {
    res.status(401).send({ error: err.message })
  }
}