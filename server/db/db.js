const { MongoClient } = require('mongodb')
const path = require('path')
require("dotenv").config({ path: path.resolve(__dirname, '../.env')})

let dbConnection;
const dbName = process.env.MONGO_URL || 'mongodb://localhost:27017/myfirst-database'

module.exports = {
  connectToDb: (cb) => {
    MongoClient.connect(dbName)
      .then(client => {
        dbConnection = client.db()
        return cb()
      })
      .catch(err => {
        console.error(`Error in MongoClient connection, error details: ${err}`)
        return cb(err)
      })
  },
  getDb: () => dbConnection
}