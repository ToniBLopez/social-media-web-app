const { ObjectId } = require('mongodb')

const collectionName = 'posts'

const postsCollection = {
  createPost: (req, res, db) => {
    try {
      const {
        authorId,
        name,
        userName,
        authorProfilePhoto,
        description,
        picturePath
      } = req.body
      const newPost = {
        _id: new ObjectId(),
        authorId,
        name,
        userName,
        authorProfilePhoto,
        description,
        picture: picturePath,
        likes: {},
        comments: [],
        // SIMULATION: (timestamps: true)
        createdAt: new Date(), // The time is 1h less than my current time
        updatedAt: new Date()
      }
      db.collection(collectionName)
        .insertOne(newPost)
        .then(result => {
          if (result.acknowledged === true) {
            db.collection(collectionName)
              .find() // cursor object
              .sort({ createdAt: -1 }) // Sort in reverse
              .toArray() // cursor method
              .then(documents => {
                res.status(200).json(documents)
              })
              .catch((err) => {
                res.status(500).json({ err })
              })
          }
        })
        .catch(err => {
          res.status(409).json({ error: err, message: 'Could not create a new document' })
        })
    } catch (err) {
      res.status(409).json({ message: err.message })
    }
  },

  getPosts: (res, db) => {
    db.collection(collectionName)
      .find()
      .sort({ createdAt: -1 })
      .toArray()
      .then(posts => {
        res.status(200).json(posts)
      })
      .catch((err) => {
        res.status(500).json({ err })
      })
  },

  getAuthorPosts: (req, res, db) => {
    try {
      const { authorId } = req.params
      if (authorId) {
        db.collection(collectionName)
          .find({ authorId: authorId })
          .sort({ createdAt: -1 })
          .toArray()
          .then(posts => {
            res.status(200).json(posts)
          })
          .catch(err => {
            res.status(500).json({ err })
          })
      } else {
        res.status(500).json({ error: 'Not a valid author' })
      }
    } catch (err) {
      res.status(405).json({ err: err.message })
    }
  },

  updateLikes: (req, res, db) => {
    const { postId } = req.params
    const { userId } = req.body
    if (ObjectId.isValid(postId) && ObjectId.isValid(userId)) {
      db.collection(collectionName)
        .findOne({ _id: ObjectId(postId) })
        .then(post => {
          const isLiked = post.likes[userId]
          if (isLiked) {
            delete post.likes[userId]
          } else {
            post.likes[userId] = true
          }
          db.collection(collectionName)
            .updateOne({ _id: ObjectId(postId) }, { $set: { likes: post.likes } })
            .then(() => {
              res.status(200).json(post)
            })
            .catch(err => {
              res.status(500).json({ error: `Could not update the document, details: ${err}` })
            })
        })
        .catch(err => {
          res.status(500).json({ err })
        })
    } else {
      res.status(500).json({ error: 'Not a valid doc ID' })
    }
  }
}

module.exports = postsCollection