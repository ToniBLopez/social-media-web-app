const { ObjectId } = require('mongodb')

const collectionName = 'users'

const usersCollection = {
  getUsers: (res, db) => {
    let documents = []
    db.collection(collectionName)
      .find()
      .sort({ createdAt: 1 }) // Ascending order
      .forEach(document => {
        delete document.password
        documents.push(document)
      })
      .then(() => {
        res.status(200).json(documents)
      })
      .catch((err) => {
        res.status(500).json({ err })
      })
  },

  getUser: (req, res, db) => {
    const endpointIdValue = req.params.id
    if (ObjectId.isValid(endpointIdValue)) {
      db.collection(collectionName)
        .findOne({ _id: ObjectId(endpointIdValue) })
        .then(doc => {
          res.status(200).json(doc)
        })
        .catch(err => {
          res.status(500).json({ err })
        })
    } else {
      res.status(500).json({ error: 'Not a valid doc id' })
    }
  },

  getUsersFollowed: (req, res, db) => {
    const followedsData = []
    const { id } = req.params
    if (ObjectId.isValid(id)) {
      db.collection(collectionName)
        .findOne({ _id: ObjectId(id) })
        .then(doc => {
          const followeds = doc.followeds
          const promises = followeds.map(followedId => {
            return db.collection(collectionName)
              .findOne({ _id: ObjectId(followedId) })
              .then(doc => {
                followedsData.push(doc)
              })
              .catch(err => {
                res.status(500).json({ err })
              })
          })
          Promise.all(promises)
            .then(() => {
              res.status(200).json(followedsData)
            })
            .catch(err => {
              res.status(500).json({ err })
            })
        })
        .catch(err => {
          res.status(500).json({ err })
        })
    } else {
      res.status(500).json({ error: 'Not a valid doc id' })
    }
  },

  /* Get random users that I don't follow */
  getRandomUsers: (req, res, db) => {
    const myOwnId = req.params.id
    if (ObjectId.isValid(myOwnId)) {
      db.collection(collectionName)
        // $sample + size = Get 50 random documents, $sort + randomField = Sort the documents randomly
        .aggregate([{ $sample: { size: 50 } }, { $sort: { randomField: 1 } }])
        .toArray()
        .then(users => {
          const usersId = users.map(user => user._id.toString())
          db.collection(collectionName)
            .findOne({ _id: ObjectId(myOwnId) })
            .then(user => {
              const followedsId = user.followeds
              const unfollowedUsers = usersId.filter(id => !followedsId.includes(id) && id !== myOwnId)
              const usersData = users.filter(user => unfollowedUsers.includes(user._id.toString()))
              res.status(200).json(usersData)
            })
            .catch(err => {
              res.status(500).json({ err })
            })
        })
        .catch(err => {
          res.status(500).json({ err })
        })
    } else {
      res.status(500).json({ error: 'Not a valid doc id' })
    }
  },

  /* UPDATE */
  updateUserFollowed: (req, res, db) => {
    const { id, followedId } = req.params
    if (ObjectId.isValid(id) && ObjectId.isValid(followedId)) {
      db.collection(collectionName)
        .findOne({ _id: ObjectId(id), followeds: followedId })
        .then(doc => {
          /* Followed exists, remove it */
          if (doc) {
            db.collection(collectionName)
              .updateOne(
                { _id: ObjectId(id) },
                { $pull: { followeds: followedId } }
              )
              .then(() => {
                res.status(200).json({ response: 'Followed Removed' })
              })
              .catch(err =>
                res.status(500).json({ error: err, message: 'It has not been possible to remove the followed' })
              )
          /* Followed doesn't exist, add it */
          } else {
            db.collection(collectionName)
              .updateOne(
                { _id: ObjectId(id) },
                { $addToSet: { followeds: followedId } }
              )
              .then(() => {
                res.status(200).json({ response: 'Followed Added' })
              })
              .catch(err =>
                res.status(500).json({ error: err, message: 'It has not been possible to add the followed' })
              )
          }
        })
        .catch(err => {
          res.status(500).json(err)
        })
    }
  }
}

module.exports = usersCollection