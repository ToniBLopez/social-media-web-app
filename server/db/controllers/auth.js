const { ObjectId } = require('mongodb')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const path = require('path')

const collectionName = 'users'
require("dotenv").config({ path: path.resolve(__dirname, '../../.env') })

const authCollection = {
  register: (req, res, db) => {
    console.log('dentro de register en el servidor')
    try {
      const {
        email,
        password,
        name,
        userName,
        birthdate,
        profilePicturePath
      } = req.body
      db.collection(collectionName)
        .findOne({ email: email })
        .then(async userExist => {
          if (userExist) {
            res.status(406).json({ error: 'This email already exists' })
          } else {
            const salt = await bcrypt.genSalt() // Two same passwords will be different
            bcrypt.hash(password, salt, (err, hash) => {
              if (err) {
                return res.status(500).json({
                  error: err.message,
                  message: 'Encryption error'
                })
              } else {
                const newUser = {
                  _id: new ObjectId(),
                  name,
                  userName,
                  email,
                  password: hash,
                  birthdate,
                  profilePicture: profilePicturePath,
                  followers: Math.floor(Math.random() * 10000),
                  followeds: [],
                  viewedProfile: Math.floor(Math.random() * 10000),
                  // SIMULATION: (timestamps: true)
                  createdAt: new Date(), // The time is 1h less than my current time
                  updatedAt: new Date()
                }
                db.collection(collectionName)
                  .insertOne(newUser)
                  .then(result => {
                    res.status(201).json(result)
                  })
                  .catch(err => {
                    res.status(409).json({
                      error: err.message,
                      message: 'Could not create a new document'
                    })
                  })
              }
            })
          }
        })
        .catch(err => {
          res.status(500).json({ error: err.message })
        })
    } catch (error) {
      res.status(500).json({ error, })
    }
  },

  login: (req, res, db) => {
    console.log('dentro de login en el servidor')
    const { email, password } = req.body
    try {
      db.collection(collectionName)
        .findOne({ email: email })
        .then(userExist => {
          if (!userExist) {
            res.status(404).json({ error: 'Email does not exist' })
          } else {
            bcrypt.compare(password, userExist.password, (err, match) => {
              if (match) { // Create a token
                // Payload of the token
                const token = jwt.sign(
                  { id: userExist._id },
                  process.env.JWT_SECRET,
                  { expiresIn: '1h' }
                )
                delete userExist.password
                res.status(200).json({
                  token,
                  user: userExist,
                })
              } else {
                res.status(406).json({ error: 'Incorrect password' })
              }
            })
          }
        })
        .catch(err => {
          res.status(500).json({ error: err.message })
        })
    } catch (err) {
      res.status(500).json({ error: err.message })
    }
  }
}

module.exports = authCollection