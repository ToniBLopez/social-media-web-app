const multer = require('multer')
const verifyToken = require('../middleware/auth')

/* FILE STORAGE */
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    let uploadPath;
    switch (req.body.type) {
      case 'post':
        uploadPath = 'public/assets/uploads/posts/';
        break;
      case 'profile':
        uploadPath = 'public/assets/uploads/profiles/';
        break;
      case 'advertising':
        uploadPath = 'public/assets/uploads/advertisements/';
        break;
    }
    cb(null, uploadPath)
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
const upload = multer({ storage })

const {
  register,
  login
} = require('../db/controllers/auth')
const {
  getUsers,
  getUser,
  getUsersFollowed,
  getRandomUsers,
  updateUserFollowed
} = require('../db/controllers/users')
const {
  createPost,
  getPosts,
  getAuthorPosts,
  updateLikes
} = require('../db/controllers/posts')

module.exports = {
  // AUTH
  auth: (app, db) => {
    app.post('/register', upload.single('profilePicture'), (req, res) => {
      register(req, res, db)
    })
    app.post('/login', (req, res) => {
      login(req, res, db)
    })
  },

  // USERS
  users: (app, db) => {
    // Get all users
    app.get('/users', verifyToken, (req, res) => {
      getUsers(res, db)
    })
    // Get a user by its Id
    app.get('/users/:id', verifyToken, (req, res) => {
      getUser(req, res, db)
    })
    // Get followeds of user
    app.get('/users/:id/followeds', verifyToken, (req, res) => {
      getUsersFollowed(req, res, db)
    })
    // Get random users that I don't follow
    app.get('/users/random/:id', verifyToken, (req, res) => {
      getRandomUsers(req, res, db)
    })
    // Add or Remove followed
    app.patch('/users/:id/:followedId', verifyToken, (req, res) => {
      updateUserFollowed(req, res, db)
    })
  },

  // POSTS
  posts: (app, db) => {
    // Get all posts
    app.get('/posts', verifyToken, (req, res) => {
      getPosts(res, db)
    })
    // Get all posts made by a user
    app.get('/posts/:authorId/posts', verifyToken, (req, res) => {
      getAuthorPosts(req, res, db)
    })
    // Create a post
    app.post('/posts', verifyToken, upload.single('picture'), (req, res) => {
      createPost(req, res, db)
    })
    // Update post likes by their Id
    app.patch('/posts/:postId/like', verifyToken, (req, res) => {
      updateLikes(req, res, db)
    })
  },
}
