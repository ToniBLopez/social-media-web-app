const express = require('express')
const path = require('path')
const helmet = require('helmet')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const cors = require('cors')
// I tell dotenv to look for the .env file inside this same folder (/server), and not in the root of the project
require("dotenv").config({ path: path.resolve(__dirname, '.env') })

const { connectToDb, getDb } = require('./db/db')
const routes = require('./routes/routes')
const app = express()
// To be able to receive the JSON requests
app.use(express.json())
// Improve app security
app.use(helmet())
app.use(helmet.crossOriginResourcePolicy({ policy: "cross-origin" }))
// Logs HTTP requests arriving to the server in the console
app.use(morgan("common"))
app.use(bodyParser.json({ limit: "30mb", extended: true }))
app.use(bodyParser.urlencoded({ limit: "30mb", extended: true }))
app.use(cors())
// Allows the use of files for webpack
app.use(express.static(path.join(__dirname, '../build')))
// Allows the use of static files, in this case images
app.use("/assets", express.static(path.join(__dirname, "../public/assets")))

const PORT = process.env.PORT || 3000
let db;

// DB CONNECTION
connectToDb(err => {
  if (!err) {
    app.listen(PORT, () => {
      console.log(`App listening on port: ${PORT}`)
      routes.auth(app, db)
      routes.users(app, db)
      routes.posts(app, db)
      app.get('*', (req, res) => {
        res.sendFile(path.join(__dirname, '../build/index.html'));
      })
    })
    db = getDb()
  }
})
