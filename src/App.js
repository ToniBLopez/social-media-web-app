import './App.scss'
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom'
import LoginPage from './scenes/loginPage'
import HomePage from './scenes/homePage'
import ProfilePage from './scenes/profilePage'
import { ThemeProvider, CssBaseline } from '@mui/material'
import { createTheme } from '@mui/material/styles';
import { themeSettings } from './theme.js'
import { useSelector } from 'react-redux'

const App = () => {
  const { mode } = useSelector(state => state)
  const theme = createTheme(themeSettings(mode), [mode])
  const isAuth = Boolean(useSelector(state => state.token))

  return (
    <>
      <BrowserRouter>
        <ThemeProvider theme={theme}>
          <CssBaseline />
          <Routes>
            <Route path='/' element={<LoginPage />} />
            <Route path='/home'>
              <Route index element={isAuth ? <HomePage /> : <Navigate to='/' />}/>
              <Route path='profile/:userId' element={isAuth ? <ProfilePage /> : <Navigate to='/' />}/>
            </Route>
          </Routes>
        </ThemeProvider>
      </BrowserRouter>
    </>
  )
}

export default App
