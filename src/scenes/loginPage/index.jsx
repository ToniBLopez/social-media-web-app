import { DESKTOP_BREAKPOINT, MOBILE_BREAKPOINT } from '../../utils/mediaQueries'
import Form from './Form.jsx'
import {
  Box,
  Typography,
  useMediaQuery,
  useTheme
} from '@mui/material'

const LoginPage = () => {
  const isDesktop = useMediaQuery(DESKTOP_BREAKPOINT)
  const isMobile = useMediaQuery(MOBILE_BREAKPOINT)
  const theme = useTheme()

  return (
    <>
      <Box
        className='header'
        sx={{
          width: '100vw',
          height: isMobile ? '80px' : '100px',
          backgroundColor: theme.palette.background.alt,
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <Typography
          variant={isMobile ? 'h2' : 'h1'}
          sx={{
            fontWeight: "bold",
            color: theme.palette.primary.main,
            '&:hover': {
              cursor: 'default'
            }
          }}
        >
          Social Media App
        </Typography>
      </Box>
      <Box>
        <Box
          sx={{
            maxWidth: '900px',
            height: 'auto',
            m: isDesktop ? '20px auto 0px auto' : '20px 20px 0px 20px',
            p: '25px 25px',
            borderRadius: '20px',
            backgroundColor: theme.palette.background.alt,
          }}>
          <Typography
            fontWeight='500'
            variant='h5'
            sx={{
              fontWeight: 'bold',
            }}
          >
            Welcome to Social Media App, the Social Media Project!
          </Typography>
          <Form />
        </Box>
      </Box>
    </>
  )
}

export default LoginPage
