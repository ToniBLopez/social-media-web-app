import {
  Box,
  Typography,
  InputAdornment,
  useTheme,
  TextField,
  Button,
  Stack,
  Divider
} from '@mui/material'
import { useState, useEffect } from 'react'
import Dropzone from 'react-dropzone'
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs';
import dayjs from 'dayjs'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { useFormik } from 'formik';
import { registerSchema, loginSchema, initialValuesRegister, initialValuesLogin } from './formValidation'
import { red } from '@mui/material/colors';
import { useNavigate } from 'react-router-dom';
import { setLogin } from "../../state";
import { useDispatch } from 'react-redux';

function Form() {
  const [pageType, setPageType] = useState('login')
  const [birthdateValue, setBirthdateValue] = useState(null)
  const [showPictureError, setShowPictureError] = useState(false)
  const [emailAddedLogin, setEmailAddedLogin] = useState(false)
  const navigate = useNavigate()
  const dispatch = useDispatch()
  const isLogin = pageType === 'login'
  const isRegister = pageType === 'register'
  const spaceBetweenElements = '15px'
  const theme = useTheme()

  const register = async (values, actions) => {
    const formData = new FormData()
    formData.append("type", 'profile')
    for (let value in values) {
      formData.append(value, values[value])
    }
    formData.append("profilePicturePath", values.profilePicture.path);
    let savedUser
    try {
      const savedUserResponse = await fetch(
        "http://localhost:3000/register",
        {
          method: 'POST',
          body: formData
        }
      )
      savedUser = await savedUserResponse.json()
      if (savedUserResponse.ok) {
        setPageType('login')
        actions.resetForm()
        console.log('Register ok')
      } else if (savedUser.error === 'This email already exists') {
        errors.email = 'This email already exists'
      } else {
        console.error(savedUser)
      }
    } catch (error) {
      console.error(error)
    }
  }

  const login = async (values, actions) => {
    let loggedIn
    try {
      const loggedInResponse = await fetch(
        'http://localhost:3000/login',
        {
          method: "POST",
          headers: { "Content-Type": "application/json" },
          body: JSON.stringify(values)
        }
      )
      loggedIn = await loggedInResponse.json()
      if (loggedInResponse.ok) {
        dispatch(
          setLogin({
            user: loggedIn.user,
            token: loggedIn.token
          })
        )
        actions.resetForm()
        navigate('/home')
      } else {
        /* Handle incorrect email or password */
        switch (loggedIn.error) {
          case 'Incorrect password':
            errors.password = 'Incorrect password'
            break;
          case 'Email does not exist':
            errors.email = 'Email does not exist'
            break;
        }
        console.error(loggedIn)
      }
    } catch (error) {
      console.error(error)
    }
  }

  const onSubmit = async (values, actions) => isLogin
    ? await login(values, actions)
    : await register(values, actions)

  const {
    values,
    errors,
    touched,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm
  } = useFormik({
    initialValues: isLogin ? initialValuesLogin : initialValuesRegister,
    validationSchema: isLogin ? loginSchema : registerSchema,
    onSubmit,
  })

  const handleNextButton = () => {
    if (!errors.email) {
      setEmailAddedLogin(true)
    }
  }
  const handleChangeEmail = () => {
    setEmailAddedLogin(false)
  }
  const handleForgotPasswordOrLogin = () => {
    if (isRegister) { // The customer has clicked on go to login
      setPageType('login')
      setBirthdateValue(null)
      setEmailAddedLogin(false)
      resetForm()
    }
  }
  const handleSignUp = () => {
    setPageType('register')
    setShowPictureError(false)
    setEmailAddedLogin(false)
    resetForm()
  }

  useEffect(() => {
    if (birthdateValue) {
      const date = birthdateValue
      setFieldValue(
        'birthdate',
        `${date.$M + 1}/${date.$D}/${date.$y}` // MM/DD/YYYY
      )
      // date.$M === Month: 0 === January
      // date.$D === Day: Is exactly the same
      // date.$y === Year: Is exactly the same
    }
  }, [birthdateValue])

  return (
    <form onSubmit={handleSubmit} encType='multipart/form-data'>
      {isRegister
        &&
        <>
          <TextField
            required
            label='Name'
            name='name'
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.name || ''}
            error={Boolean(touched.name) && Boolean(errors.name)}
            helperText={touched.name && errors.name}
            variant='outlined'
            sx={{
              width: '100%',
              mt: spaceBetweenElements
            }}
          />

          <Stack style={{ marginTop: spaceBetweenElements }}>
            <LocalizationProvider dateAdapter={AdapterDayjs}>
              <DatePicker
                disableFuture
                label="Birthdate"
                name='birthdate'
                onBlur={handleBlur}
                openTo="year"
                views={['year', 'month', 'day']}
                onChange={(newValue) => {
                  setBirthdateValue(newValue)
                }}
                value={birthdateValue}
                renderInput={(params) => <TextField required {...params} />}
                minDate={dayjs('1900-01-01')}
                maxDate={dayjs()}
              />
              {errors.birthdate
                !== 'birthdate must be a `date` type, but the final value was: `Invalid Date` (cast from the value `"NaN/NaN/NaN"`).'
                && birthdateValue
                &&
                <div className='handleBirthdateError'>{errors.birthdate}</div>
              }
            </LocalizationProvider>
          </Stack>

          <TextField
            required
            label='User Name'
            name='userName'
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.userName || ''}
            error={Boolean(touched.userName) && Boolean(errors.userName)}
            helperText={touched.userName && errors.userName}
            variant='outlined'
            sx={{
              width: '100%',
              mt: spaceBetweenElements
            }}
          />

          <Box
            sx={{
              display: 'inline-block',
              textAlign: 'center',
              width: '100%',
              mt: spaceBetweenElements,
              p: '14px 14px',
              border: `1px solid ${theme.palette.neutral.medium}`,
              borderRadius: '5px'
            }}
          >
            <Dropzone
              acceptedFiles=".jpg,.jpeg,.png"
              multiple={false}
              onDrop={acceptedFiles => {
                setFieldValue('profilePicture', acceptedFiles[0]) // Set the value of the file in the Yup formik schema
                setShowPictureError(true)
              }}
            >
              {({ getRootProps, getInputProps, isDragActive }) => (
                <Box
                  {...getRootProps()}
                  sx={{
                    "&:hover": { cursor: "pointer" },
                    border: showPictureError && errors.profilePicture
                      ? `2px dashed red`
                      : `2px dashed ${theme.palette.primary.main}`,
                    p: '10px'
                  }}
                >
                  <input {...getInputProps()} />
                  {isDragActive
                    ? <p>Drop the File Here</p>
                    : values.profilePicture && !errors.profilePicture
                      ? <p>{`File Accepted - ${values.profilePicture.path} (${values.profilePicture.size} bytes)`}</p>
                      : showPictureError
                        ? <p style={{ color: red[400] }}>{errors.profilePicture}</p>
                        : <p style={{ color: theme.palette.neutral.main }}>Add Picture Here</p>
                  }
                </Box>
              )}
            </Dropzone>
          </Box>
        </>
      }

      <Box sx={{ display: 'flex', alignItems: 'center' }}>
        <TextField
          required
          disabled={emailAddedLogin}
          label='Email'
          name='email'
          onBlur={handleBlur}
          onChange={handleChange}
          value={values.email}
          error={Boolean(touched.email) && Boolean(errors.email)}
          helperText={touched.email && errors.email}
          type='email'
          variant='outlined'
          sx={{
            width: '100%',
            mt: spaceBetweenElements
          }}
          InputProps={{
            endAdornment: emailAddedLogin
              &&
              <InputAdornment position="end">
                <Typography
                  onClick={handleChangeEmail}
                  sx={{
                    color: theme.palette.primary.main,
                    '&:hover': {
                      cursor: 'pointer'
                    }
                  }}
                >
                  Change
                </Typography>
              </InputAdornment>
          }}
        />
      </Box>
      {(emailAddedLogin || isRegister)
        &&
        <TextField
          required
          label='Password'
          name='password'
          onBlur={handleBlur}
          onChange={handleChange}
          value={values.password}
          error={Boolean(touched.password) && Boolean(errors.password)}
          helperText={touched.password && errors.password}
          type='password'
          variant='outlined'
          sx={{
            width: '100%',
            mt: spaceBetweenElements
          }}
        />
      }

      {isLogin && !emailAddedLogin
        &&
        <Button
          disabled={!Boolean(values.email)}
          onClick={handleNextButton}
          variant='contained'
          sx={{
            width: '100%',
            color: theme.palette.background.alt,
            backgroundColor: theme.palette.primary.main,
            fontSize: theme.typography.h5,
            fontWeight: 'bold',
            m: '15px 0px 10px 0px',
            boxShadow: 'none',
            textTransform: 'none',
            '&:hover': {
              color: theme.palette.primary.main,
              backgroundColor: theme.palette.background.alt,
              boxShadow: 'none'
            }
          }}
        >
          Next
        </Button>
      }

      {(emailAddedLogin || isRegister)
        &&
        <>
          <Button
            type='submit'
            variant='contained'
            onClick={() => setShowPictureError(true)}
            sx={{
              width: '100%',
              backgroundColor: theme.palette.primary.main,
              color: theme.palette.background.alt,
              fontSize: theme.typography.h5,
              fontWeight: 'bold',
              m: '15px 0px 10px 0px',
              boxShadow: 'none',
              textTransform: 'none',
              '&:hover': {
                color: theme.palette.primary.main,
                backgroundColor: theme.palette.background.alt,
                boxShadow: 'none'
              }
            }}
          >
            {isLogin
              ? 'Log in'
              : 'Register'}
          </Button>

          <Typography
            onClick={handleForgotPasswordOrLogin}
            title={isLogin ? 'Functionality not Implemented' : null}
            sx={{
              display: 'inline-block',
              color: theme.palette.primary.main,
              '&:hover': isRegister
                ? {
                  cursor: 'pointer',
                  textDecoration: 'underline'
                }
                : {
                  cursor: 'default'
                }
            }}
          >
            {isLogin
              ? 'Forgot Password?'
              : 'Already have an account? Log in here.'}
          </Typography>
        </>
      }

      {isLogin
        &&
        <>
          <Divider sx={{
            width: '70%',
            m: '15px 0px 25px 15%'
          }} />
          <Button
            onClick={handleSignUp}
            variant='contained'
            sx={{
              width: '50%',
              ml: '25%',
              color: theme.palette.background.alt,
              backgroundColor: '#2BCC45',
              fontSize: theme.typography.h5,
              fontWeight: 'bold',
              boxShadow: 'none',
              textTransform: 'none',
              '&:hover': {
                color: '#2BCC45',
                backgroundColor: theme.palette.background.alt,
                boxShadow: 'none'
              }
            }}
          >
            Sign up
          </Button>
        </>
      }
    </form >
  )
}

export default Form