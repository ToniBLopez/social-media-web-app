import * as yup from 'yup'

const emailRules = /\S+@\S+\.\S+/ // Must contain at least one @ symbol and one punctuation mark (text@text.text)
const passwordRules = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=\S+$).{5,}$/ // Must contain a min of 5 characters, 1 uppercase letter, 1 lowercase letter, 1 numeric digit
const nameRules = /^[a-zA-ZáéíóúÁÉÍÓÚàèìòùÀÈÌÒÙñÑ,-. ]+$/ // Can only contain letters, including accented letters and ñ, commas and dots
const userNameRules = /^[\w\p{P}|]+$/u // Can only contain letters, numbers, underscores and punctuation marks
const oneLetterRule = /^(?=.*[a-zA-Z])/ // Must contain at least one letter

export const registerSchema = yup.object().shape({
  name: yup
    .string()
    .matches(nameRules, 'Name can only contain letters, including accented letters and ñ, commas and dots')
    .matches(oneLetterRule, 'Name must contain at least one letter')
    .required('Required'),
  birthdate: yup
    .date()
    .max(new Date(), 'Birthdate must be in the past')
    .min(new Date('1900-01-01'), 'Birthdate is too old')
    .required('Required'),
  userName: yup
    .string()
    .min(3, 'User name must be at least 3 characters')
    .max(20, 'User name must be at most 20 characters')
    .matches(userNameRules, 'User name can only contain letters, numbers, underscores and and punctuation marks')
    .matches(oneLetterRule, 'User name must contain at least one letter')
    .required('Required'),
  profilePicture: yup
    .mixed()
    .test('fileType',
      'Invalid file format, it must be jpg, jpeg or png',
      (value) => {
        if (!value) return false
        return ['image/jpeg', 'image/png'].includes(value.type)
      })
    .test('fileSize',
      'File size is too large',
      (value) => {
        if (!value) return false
        return value.size <= 5000000 // 5MB in bytes
      })
    .required('Required'),
  email: yup
    .string()
    .email('Please enter a valid email')
    .matches(emailRules, 'Please enter a valid email')
    .required('Required'),
  password: yup
    .string()
    .min(6)
    .matches(passwordRules, 'Must contain a min of 5 characters, 1 uppercase letter, 1 lowercase letter, 1 numeric digit and no spaces')
    .required('Required')
})
export const loginSchema = yup.object().shape({
  email: yup
    .string()
    .email('Please enter a valid email')
    .matches(emailRules, 'Please enter a valid email')
    .required('Required'),
  password: yup
    .string()
    .min(6)
    .matches(passwordRules, { message: 'Must contain a min of 5 characters, 1 uppercase letter, 1 lowercase letter, 1 numeric digit and no spaces' })
    .required('Required')
})

export const initialValuesRegister = {
  name: '',
  birthdate: '',
  userName: '',
  profilePicture: '',
  email: '',
  password: ''
}
export const initialValuesLogin = {
  email: '',
  password: ''
}