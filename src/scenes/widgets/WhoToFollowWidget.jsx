import { Box, Typography, useTheme } from "@mui/material";
import User from "../../components/User";
import WidgetWrapper from "../../components/WidgetWrapper";
import { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setWhoToFollow } from "../../state";

const WhoToFollowWidget = () => {
  const [isLoading, setIsLoading] = useState(true)
  const { palette } = useTheme()
  const { token, whoToFollow } = useSelector((state) => state)
  const { _id } = useSelector((state) => state.user)
  const dispatch = useDispatch()

  const getRandomUsers = async () => {
    const response = await fetch(
      `http://localhost:3000/users/random/${_id}`,
      {
        method: "GET",
        headers: { Authorization: `Bearer ${token}` },
      }
    );
    const data = await response.json();
    dispatch(setWhoToFollow({ whoToFollow: data }))
    if (response.ok) {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getRandomUsers()
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <WidgetWrapper paddingBottom='1.5rem'>
      <Typography
        color={palette.neutral.dark}
        variant="h5"
        fontWeight="500"
        sx={{ mb: "1.5rem" }}
      >
        Who to follow
      </Typography>
      <Box display="flex" flexDirection="column" gap="1.5rem">
        {isLoading
          ?
          <Typography margin='1rem 0' fontWeight='bold'>
            Loading posts...
          </Typography>
          :
          whoToFollow.slice(0, 3).map((user) => (
            <User
              key={user._id}
              userId={user._id}
              name={user.name}
              userName={user.userName}
              userPicturePath={user.profilePicture}
            />
          ))}
      </Box>
    </WidgetWrapper>
  );
};

export default WhoToFollowWidget;
