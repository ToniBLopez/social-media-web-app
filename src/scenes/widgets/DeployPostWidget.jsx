import { Box, useTheme } from "@mui/material"
import AddCircleIcon from '@mui/icons-material/addCircle'
import RemoveCircleIcon from '@mui/icons-material/RemoveCircle'

const DeployPostWidget = ({ isOpen }) => {
  const { palette } = useTheme()
  const iconStyles = {
    position: 'fixed',
    bottom: '2rem',
    right: '2rem',
    width: '60px',
    height: '60px',
    color: isOpen ? palette.primary.dark : palette.primary.main,
    '&:hover': {
      cursor: 'pointer'
    }
  }

  return (
    <Box>
      {isOpen
        ?
        <RemoveCircleIcon sx={iconStyles} />
        :
        <AddCircleIcon sx={iconStyles} />
      }
    </Box>
  )
}

export default DeployPostWidget