import {
  ManageAccountsOutlined,
  EditOutlined
} from '@mui/icons-material';
import { Box, Typography, Divider, useTheme, useMediaQuery } from '@mui/material';
import { DESKTOP_BREAKPOINT } from '../../utils/mediaQueries'
import UserImage from '../../components/UserImage'
import FlexBetween from '../../components/FlexBetween';
import WidgetWrapper from '../../components/WidgetWrapper';
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function UserWidget({ userId, picturePath }) {
  const [user, setUser] = useState(null)
  const { palette } = useTheme()
  const navigate = useNavigate()
  const { token } = useSelector(state => state)
  const isDesktop = useMediaQuery(DESKTOP_BREAKPOINT)
  const primary = palette.primary.main
  const dark = palette.neutral.dark;
  const medium = palette.neutral.medium;
  const main = palette.neutral.main;

  const getUser = async () => {
    const response = await fetch(`http://localhost:3000/users/${userId}`, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` }
    })
    const data = await response.json()
    setUser(data)
  }

  useEffect(() => {
    getUser()
  }, [])

  if (!user) {
    return null
  }

  const {
    name,
    userName,
    followers,
    viewedProfile
  } = user;

  return (
    <WidgetWrapper sx={{ minWidth: isDesktop ? '280px' : 'auto' }}>
      {/* FIRST ROW */}
      <FlexBetween
        gap='0.5rem'
        pb='1.1rem'
      >
        <FlexBetween
          gap='1rem'
          onClick={() => navigate(`/home/profile/${userId}`)}
        >
          <UserImage image={picturePath} />
          <Box>
            <Typography
              variant='h4'
              color={dark}
              fontWeight='500'
              sx={{
                transition: 'color 0.1s ease-in-out',
                '&:hover': {
                  color: primary,
                  cursor: 'pointer'
                },
              }}
            >
              {name}
            </Typography>
            <Typography color={medium}>@{userName}</Typography>
          </Box>
        </FlexBetween>
        <ManageAccountsOutlined title='TITLE' />
      </FlexBetween>

      <Divider />

      {/* SECOND ROW */}
      <Box p='1rem 0'>
        <FlexBetween mb='0.5rem'>
          <Typography color={medium}>Followers</Typography>
          <Typography color={dark} sx={{ fontWeight: 'bold' }}>
            {followers ? followers : 0}
          </Typography>
        </FlexBetween>
        <FlexBetween>
          <Typography color={medium}>Profile visits this month</Typography>
          <Typography color={dark} sx={{ fontWeight: 'bold' }}>
            {viewedProfile}
          </Typography>
        </FlexBetween>
      </Box>

      <Divider />

      {/* THIRD ROW */}
      <Box p='1rem 0'>
        <Typography fontSize='1rem' color={main} fontWeight='500' mb='1rem'>
          Social Profiles
        </Typography>

        <FlexBetween gap='1rem' mb='0.5rem'>
          <FlexBetween gap='1rem'>
            <img className='socialMediaIcon' src='./../../assets/socialMediaIcons/twitter-120.png' alt='twitter' />
            <Box>
              <Typography color={main} fontWeight='500'>
                Twitter
              </Typography>
              <Typography color={medium}>Social Network</Typography>
            </Box>
          </FlexBetween>
          <EditOutlined sx={{ color: main }} />
        </FlexBetween>

        <FlexBetween gap='1rem'>
          <FlexBetween gap='1rem'>
            <img className='socialMediaIcon' src='../../../assets/socialMediaIcons/linkedin-120.png' alt='linkedin' />
            <Box>
              <Typography color={main} fontWeight='500'>
                Linkedin
              </Typography>
              <Typography color={medium}>Network Platform</Typography>
            </Box>
          </FlexBetween>
          <EditOutlined sx={{ color: main }} />
        </FlexBetween>

      </Box>
    </WidgetWrapper>
  )
}

export default UserWidget