import {
  EditOutlined,
  DeleteOutlined,
  AttachFileOutlined,
  ImageOutlined
} from "@mui/icons-material";
import {
  Box,
  Divider,
  Typography,
  InputBase,
  useTheme,
  Button,
  IconButton,
  useMediaQuery,
} from "@mui/material";
import { DESKTOP_BREAKPOINT } from '../../utils/mediaQueries'
import FlexBetween from "../../components/FlexBetween";
import Dropzone from "react-dropzone";
import UserImage from "../../components/UserImage";
import WidgetWrapper from "../../components/WidgetWrapper";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setPosts } from "../../state";
import { useNavigate } from 'react-router-dom';

const MyPostWidget = ({ picturePath, isOpen }) => {
  const [isImage, setIsImage] = useState(false)
  const [image, setImage] = useState(null)
  const [text, setText] = useState("")
  const dispatch = useDispatch()
  const navigate = useNavigate()
  const { palette } = useTheme()
  const { _id, name, userName, profilePicture, } = useSelector((state) => state.user);
  const { token } = useSelector((state) => state);
  const isDesktop = useMediaQuery(DESKTOP_BREAKPOINT);
  const mediumMain = palette.neutral.mediumMain;
  const medium = palette.neutral.medium;
  const primary = palette.primary.main

  const handlePost = async () => {
    const formData = new FormData();
    formData.append("type", 'post');
    formData.append("authorId", _id);
    formData.append("name", name);
    formData.append("userName", userName);
    formData.append("authorProfilePhoto", profilePicture);
    formData.append("description", text);
    if (image) {
      formData.append("picture", image);
      formData.append("picturePath", image.name);
    }

    const response = await fetch(`http://localhost:3000/posts`, {
      method: "POST",
      headers: { Authorization: `Bearer ${token}` },
      body: formData
    });
    const posts = await response.json();

    if (response.ok) {
      dispatch(setPosts({ posts }))
      setImage(null);
      setText("");
    } else {
      console.error(posts)
    }
  };

  return (
    <WidgetWrapper
      display={isOpen ? 'block' : 'none'}
      sx={
        !isDesktop
        &&
        {
          position: 'fixed',
          zIndex: '1',
          width: '88%',
          right: '6%',
          bottom: '5rem',
          border: `solid 2px ${palette.primary.dark}`
        }
      }
      mb='2rem'
    >
      <FlexBetween gap="1.5rem">
        <Box
          onClick={() => {
            navigate(`/home/profile/${_id}`)
            navigate(0)
          }}
        >
          <UserImage image={picturePath} />
        </Box>
        <InputBase
          placeholder="What's on your mind..."
          onChange={(e) => setText(e.target.value)}
          value={text}
          sx={{
            width: "100%",
            backgroundColor: palette.neutral.light,
            borderRadius: "2rem",
            padding: "1rem 2rem",
          }}
        />
      </FlexBetween>
      {isImage
        &&
        <Box
          textAlign='center'
          border={`1px solid ${medium}`}
          borderRadius="5px"
          mt="1rem"
          p="1rem"
        >
          <Dropzone
            acceptedFiles=".jpg,.jpeg,.png"
            multiple={false}
            onDrop={(acceptedFiles) => setImage(acceptedFiles[0])}
          >
            {({ getRootProps, getInputProps }) => (
              <FlexBetween>
                <Box
                  {...getRootProps()}
                  border={`2px dashed ${primary}`}
                  p="1rem"
                  width="100%"
                  sx={{ "&:hover": { cursor: "pointer" } }}
                >
                  <input {...getInputProps()} />
                  {!image ? (
                    <p>Add Image Here</p>
                  ) : (
                    <FlexBetween>
                      <Typography>{image.name}</Typography>
                      <EditOutlined />
                    </FlexBetween>
                  )}
                </Box>
                {image && (
                  <IconButton
                    onClick={() => setImage(null)}
                    sx={{ width: "15%" }}
                  >
                    <DeleteOutlined />
                  </IconButton>
                )}
              </FlexBetween>
            )}
          </Dropzone>
        </Box>
      }

      <Divider sx={{ margin: "1.25rem 0" }} />

      <FlexBetween>
        <FlexBetween gap="0.25rem" onClick={() => setIsImage(!isImage)}>
          <ImageOutlined sx={{ color: mediumMain }} />
          <Typography
            color={mediumMain}
            sx={{ "&:hover": { cursor: "pointer", color: medium } }}
          >
            Image
          </Typography>
        </FlexBetween>
        <FlexBetween title='Functionality not Implemented' gap="0.25rem">
          <AttachFileOutlined sx={{ color: mediumMain }} />
          <Typography color={mediumMain}>Attachment</Typography>
        </FlexBetween>

        <Button
          disabled={!text}
          onClick={handlePost}
          sx={{
            fontWeight: 'bold',
            color: palette.background.alt,
            backgroundColor: primary,
            borderRadius: "3rem",
            "&:hover": {
              bgcolor: palette.background.alt,
              color: primary
            }
          }}
        >
          POST
        </Button>
      </FlexBetween>
    </WidgetWrapper >
  );
};

export default MyPostWidget;
