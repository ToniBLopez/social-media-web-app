import { Box, Typography, useTheme } from "@mui/material";
import User from "../../components/User";
import WidgetWrapper from "../../components/WidgetWrapper";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setFolloweds } from "../../state";

const FollowedsListWidget = ({ userId }) => {
  const dispatch = useDispatch()
  const { palette } = useTheme()
  const { token } = useSelector((state) => state);
  const { _id, followeds } = useSelector((state) => state.user);
  const [profileFolloweds, setProfileFolloweds] = useState([])
  const itsMe = _id === userId

  const getProfileFolloweds = async () => {
    const response = await fetch(
      `http://localhost:3000/users/${userId}/followeds`,
      {
        method: "GET",
        headers: { Authorization: `Bearer ${token}` },
      }
    )
    const data = await response.json()
    setProfileFolloweds(data)
  }

  const updateFolloweds = async () => {
    const response = await fetch(
      `http://localhost:3000/users/${_id}/followeds`,
      {
        method: "GET",
        headers: { Authorization: `Bearer ${token}` },
      }
    );
    const data = await response.json()
    dispatch(setFolloweds({ followeds: data }))
  }

  useEffect(() => {
    getProfileFolloweds();
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    updateFolloweds()
  }, [profileFolloweds])

  return (
    <WidgetWrapper>
      <Typography
        color={palette.neutral.dark}
        variant="h5"
        fontWeight="500"
        sx={{ mb: "1.5rem" }}
      >
        Followeds List
      </Typography>
      <Box display="flex" flexDirection="column" gap="1.5rem">
        {(itsMe ? followeds : profileFolloweds)
          .slice(0, 3).map((followed) => (
            <User
              key={followed._id}
              userId={followed._id}
              name={followed.name}
              userName={followed.userName}
              userPicturePath={followed.profilePicture}
            />
          ))}
      </Box>
      <Box display="flex" justifyContent='end' >
        <Typography
          color={palette.primary.main}
          sx={{
            mt: '1rem',
            cursor: 'default'
          }}
        >
          Show more
        </Typography>
      </Box>
    </WidgetWrapper>
  );
};

export default FollowedsListWidget;
