import { Typography } from '@mui/material';
import { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setPosts, setFolloweds } from '../../state';
import PostWidget from './PostWidget';

const PostsWidget = ({ authorId, isProfile = false }) => {
  const [isLoading, setIsLoading] = useState(true)
  const dispatch = useDispatch()
  const { token, posts } = useSelector((state) => state)
  const { _id } = useSelector((state) => state.user)

  const getPosts = async () => {
    const url = isProfile
      ?
      `http://localhost:3000/posts/${authorId}/posts`
      :
      'http://localhost:3000/posts'
    const response = await fetch(url, {
      method: 'GET',
      headers: { Authorization: `Bearer ${token}` },
    });
    const data = await response.json()
    dispatch(setPosts({ posts: data }))
    setIsLoading(false)
  };

  const updateFolloweds = async () => {
    const response = await fetch(
      `http://localhost:3000/users/${_id}/followeds`,
      {
        method: "GET",
        headers: { Authorization: `Bearer ${token}` },
      }
    );
    const data = await response.json()
    dispatch(setFolloweds({ followeds: data }))
  }

  useEffect(() => {
    getPosts()
    updateFolloweds()
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      {isLoading
        ?
        <Typography margin='2rem 0' fontWeight='bold'>
          Loading posts...
        </Typography>
        :
        posts.map(
          ({
            _id,
            authorId,
            name,
            userName,
            authorProfilePhoto,
            description,
            picture,
            likes,
            comments,
          }) => (
            <PostWidget
              key={_id}
              postId={_id}
              postUserId={authorId}
              name={name}
              userName={userName}
              userPicturePath={authorProfilePhoto}
              description={description}
              picturePath={picture}
              likes={likes}
              comments={comments}
              isProfile
            />
          )
        )}
    </>
  );
};

export default PostsWidget;
