import { Box, Button, useMediaQuery, useTheme } from "@mui/material";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Navbar from '../navbar'
import FollowedsListWidget from "../widgets/FollowedsListWidget";
import MyPostWidget from "../../scenes/widgets/MyPostWidget";
import PostsWidget from "../../scenes/widgets/PostsWidget";
import UserWidget from "../../scenes/widgets/UserWidget";
import DeployPostWidget from "../../scenes/widgets/DeployPostWidget";
import { DESKTOP_BREAKPOINT } from '../../utils/mediaQueries'

function ProfilePage() {
  const [user, setUser] = useState(null)
  const [showAddPost, setShowAddPost] = useState(false)
  const [isOpen, setIsOpen] = useState(false)
  const { userId } = useParams()
  const { token } = useSelector((state) => state);
  const { profilePicture } = useSelector((state) => state.user);
  const { palette } = useTheme()
  const isDesktop = useMediaQuery(DESKTOP_BREAKPOINT);

  const getUser = async () => {
    const response = await fetch(`http://localhost:3000/users/${userId}`, {
      method: "GET",
      headers: { Authorization: `Bearer ${token}` },
    });
    const data = await response.json();
    setUser(data);
  }

  useEffect(() => {
    getUser();
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  if (!user) return null;

  return (
    <Box>
      <Navbar />
      <Box
        width="100%"
        padding="2rem 6%"
        display={isDesktop ? "flex" : "block"}
        gap="2rem"
        justifyContent="center"
      >
        <Box flexBasis={isDesktop ? "26%" : undefined}>
          <UserWidget userId={userId} picturePath={user.profilePicture} />
          {isDesktop &&
            <Box display="flex" justifyContent='end'>
              <Button
                onClick={() => showAddPost ? setShowAddPost(false) : setShowAddPost(true)}
                sx={{
                  width: '40%',
                  mt: '15px',
                  fontWeight: 'bold',
                  color: palette.background.default,
                  backgroundColor: palette.primary.main,
                  borderRadius: "3rem",
                  "&:hover": {
                    bgcolor: palette.background.default,
                    color: palette.primary.main
                  }
                }}
              >
                {showAddPost ? 'HIDE POST' : 'ADD POST'}
              </Button>
            </Box>
          }
          <Box m="2rem 0" />
        </Box>
        <Box
          flexBasis={isDesktop ? "42%" : undefined}
          mt={isDesktop ? undefined : "2rem"}
        >
          <MyPostWidget
            picturePath={profilePicture}
            isOpen={
              showAddPost
                ? true
                : !isDesktop
                  &&
                  isOpen
            }
          />
          {!isDesktop
            &&
            <Box m="2rem 0" />
          }
          <PostsWidget authorId={userId} isProfile />
        </Box>
        <Box flexBasis={isDesktop ? "26%" : undefined}>
          <FollowedsListWidget userId={userId} />
        </Box>
      </Box>
      {!isDesktop
        &&
        <Box onClick={() => setIsOpen(!isOpen)}>
          <DeployPostWidget isOpen={isOpen} />
        </Box>
      }
    </Box>
  )
}

export default ProfilePage