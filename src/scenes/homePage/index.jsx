import { Box, useMediaQuery } from '@mui/material'
import { DESKTOP_BREAKPOINT } from '../../utils/mediaQueries'
import { useState } from 'react'
import { useSelector } from 'react-redux'
import Navbar from '../../scenes/navbar'
import UserWidget from "../widgets/UserWidget";
import MyPostWidget from "../widgets/MyPostWidget";
import PostsWidget from "../widgets/PostsWidget";
import WhoToFollowWidget from "../widgets/WhoToFollowWidget";
import AdvertWidget from "../widgets/AdvertWidget";
import DeployPostWidget from "../widgets/DeployPostWidget";

function HomePage() {
  const [isOpen, setIsOpen] = useState(false)
  const { _id, profilePicture } = useSelector(state => state.user)
  const isDesktop = useMediaQuery(DESKTOP_BREAKPOINT)

  return (
    <Box>
      <Navbar />
      <Box
        width="100%"
        padding="2rem 6%"
        display={isDesktop ? "flex" : "block"}
        gap="0.5rem"
        justifyContent="space-between"
      >
        <Box flexBasis={isDesktop ? "26%" : undefined}>
          <UserWidget userId={_id} picturePath={profilePicture} />
        </Box>
        <Box
          flexBasis={isDesktop ? "42%" : undefined}
          mt={isDesktop ? undefined : "2rem"}
        >
          <MyPostWidget picturePath={profilePicture} isOpen={isDesktop ? true : isOpen} />
          <PostsWidget authorId={_id} />
        </Box>
        <Box
          flexBasis={isDesktop ? "26%" : undefined}
          mt={isDesktop ? undefined : "2rem"}
        >
          <AdvertWidget />
          <Box m="2rem 0" />
          <WhoToFollowWidget />
        </Box>
      </Box>
      {!isDesktop
        &&
        <Box onClick={() => setIsOpen(!isOpen)}>
          <DeployPostWidget isOpen={isOpen} />
        </Box>
      }
    </Box>
  )
}

export default HomePage