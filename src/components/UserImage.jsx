import { Box } from "@mui/material";

const UserImage = ({ image, size = "60px" }) => {

  return (
    <Box
      width={size}
      height={size}
      sx={{
        transition: 'opacity 0.1s ease-in-out',
        ":hover": {
          cursor: 'pointer',
          opacity: 0.8
        }
      }}
    >
      <img
        style={{ objectFit: "cover", borderRadius: "50%" }}
        width={size}
        height={size}
        alt="User Image"
        src={`http://localhost:3000/assets/uploads/profiles/${image}`}
      />
    </Box>
  );
};

export default UserImage;
