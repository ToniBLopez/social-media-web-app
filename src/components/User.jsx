import { PersonAddOutlined, PersonRemoveOutlined } from "@mui/icons-material";
import { Box, IconButton, Typography, useTheme } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setFolloweds, setWhoToFollow } from "../state";
import FlexBetween from "./FlexBetween";
import UserImage from "./UserImage";

const User = ({ userId, name, userName, userPicturePath }) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { _id, followeds } = useSelector((state) => state.user);
  const { token } = useSelector((state) => state);
  const { palette } = useTheme();
  const primaryLight = palette.primary.light;
  const primaryDark = palette.primary.dark;
  const main = palette.neutral.main;
  const medium = palette.neutral.medium;
  const isFollowed = followeds.find((followed) => followed._id === userId);

  const patchFollowed = async () => {
    const patchResponse = await fetch(
      `http://localhost:3000/users/${_id}/${userId}`,
      {
        method: "PATCH",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }
    )

    /* UPDATE WHOTOFOLLOW */
    const whoToFollowResponse = await fetch(
      `http://localhost:3000/users/random/${_id}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
          "Content-Type": "application/json",
        },
      }
    );
    const woToFollowData = await whoToFollowResponse.json();
    dispatch(setWhoToFollow({ whoToFollow: woToFollowData }));

    if (patchResponse.ok) {
      const followedsResponse = await fetch(
        `http://localhost:3000/users/${_id}/followeds`,
        {
          method: "GET",
          headers: {
            Authorization: `Bearer ${token}`,
            "Content-Type": "application/json",
          },
        }
      );
      const followedsData = await followedsResponse.json();
      dispatch(setFolloweds({ followeds: followedsData }));
    } else {
      console.error('Update Denied')
    }
  }

  return (
    <FlexBetween>
      <FlexBetween
        gap="1rem"
        onClick={() => {
          navigate(`/home/profile/${userId}`)
          navigate(0)
        }}
      >
        <UserImage image={userPicturePath} size="55px" />
        <Box
        >
          <Typography
            color={main}
            variant="h5"
            fontWeight="500"
            sx={{
              "&:hover": {
                color: palette.primary.main,
                opacity: '0.8',
                cursor: "pointer",
              },
            }}
          >
            {name}
          </Typography>
          <Typography color={medium} fontSize="0.75rem">
            @{userName}
          </Typography>
        </Box>
      </FlexBetween>
      <IconButton
        onClick={() => patchFollowed()}
        sx={{ backgroundColor: primaryLight, p: "0.6rem" }}
      >
        {isFollowed ? (
          <PersonRemoveOutlined sx={{ color: primaryDark }} />
        ) : (
          <PersonAddOutlined sx={{ color: primaryDark }} />
        )}
      </IconButton>
    </FlexBetween>
  );
};

export default User;
