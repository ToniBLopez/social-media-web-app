import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  mode: "light",
  user: null,
  token: null,
  posts: [],
  whoToFollow: [],
};

const authSlice = createSlice({
  name: 'auth',
  initialState, // Defines the initial state of the slice
  reducers: { // Is an object that defines the reducer functions that will be used to update the state of the slice.
    setMode: (state) => {
      state.mode = state.mode === 'light' ? 'dark' : 'light'
    },
    setLogin: (state, action) => {
      state.user = action.payload.user
      state.token = action.payload.token
    },
    setLogout: (state) => {
      state.user = null
      state.token = null
    },
    setFolloweds: (state, action) => {
      if (state.user) {
        state.user.followeds = action.payload.followeds
      } else {
        console.error('user followeds non-existent')
      }
    },
    setPosts: (state, action) => {
      state.posts = action.payload.posts
    },
    setPost: (state, action) => {
      const updatedPost = state.posts.map(post => {
        if (post._id === action.payload.post._id) return action.payload.post
        return post
      })
      state.posts = updatedPost
    },
    setWhoToFollow: (state, action) => {
      state.whoToFollow = action.payload.whoToFollow
    }
  }
})

export const {
  setMode,
  setLogin,
  setLogout,
  setFolloweds,
  setPosts,
  setPost,
  setWhoToFollow
} = authSlice.actions;
export default authSlice.reducer;