export const MOBILE_BREAKPOINT = '(max-width:400px)'
export const TABLET_BREAKPOINT = '(min-width:400px) and (max-width:1000px)'
export const DESKTOP_BREAKPOINT = '(min-width:1000px)'
