# Social Media Project
I have created this application to show my knowledge about the development of FullStack web apps.

## Description
- It is a FullStack project, fully responsive, with dark mode and light mode and with a modern UX/UI.
- The first thing you find is a modern form to register and log in, with an UX/UI that makes you feel that the application is alive, in an elegant and simple way.
- Second, once inside, I have added an algorithm to find random people to whom you do not follow and show them in the widget: Who to follow.

#### This project is built with the MERN stack, making use of:
- [**M**ongoDB](https://www.mongodb.com/)
- [**E**xpress](https://expressjs.com/)
- [**R**eact](https://reactjs.org/)
- [**N**ode.js](https://nodejs.org/)

##### Frontend Part
- I have designed and built from scratch the style of the Frontend of the application using Figma: [See the Figma project here](https://www.figma.com/file/GvhFMjkPIVvp92fT2k7kdO/Social-Media-App?node-id=0%3A1&t=YiQYBXlo7jQdxdk6-1)
- The Frontend part is developed with [MUI](https://mui.com/) and [Sass](https://sass-lang.com/), the CSS preprocessor.
- I have used [Yup](https://www.npmjs.com/package/yup) to validate the input data of the form and [Formik](https://formik.org/docs/overview) to manage the state of the same.
- To add the user's profile picture in the form I've used [React-Dropzone](https://react-dropzone.js.org/).

##### Backend Part
- [MongoDB](https://www.mongodb.com/) using **MongoClient**, a low level library (that means you need to write code explicitly. It is easier to use Mongoose than MongoClient).
- [Webpack](https://webpack.js.org/) to bundle and compile the code.
- [Jwt](https://jwt.io/) for customer authentication and authorization.
- [Bcrypt](https://www.npmjs.com/package/bcrypt) to encrypt and validate the password.
- [Multer](https://www.npmjs.com/package/multer) to handle and allow users to upload files.

## Visuals
##### Desktop
![Desktop](https://media.giphy.com/media/7NGdooRaK32plvbH4c/giphy.gif)
##### Mobile
![Mobile](https://media.giphy.com/media/qRD85SnNo56H35wt25/giphy.gif)

## Installation
The versions used in this project is:
- node v18.12.1
- yarn v1.22.19
- mongosh v1.6.1
- corepack v0.14.2

The project is created using [Yarn](https://yarnpkg.com/) as a replacement for NPM, so you must have Yarn installed on your computer (see it below)

Clone this repository to your local machine
`git clone https://gitlab.com/ToniBLopez/social-media-web-app.git`

Rename .env.example in the server folder to .env and add your MongoDB url in MONGO_URL=...

#### Install Yarn
The preferred way to manage Yarn is through [Corepack](https://nodejs.org/dist/latest/docs/api/corepack.html), a new binary shipped with all Node.js releases starting from 16.10. It acts as an intermediary between you and Yarn, and lets you use different package manager versions across multiple projects without having to check-in the Yarn binary anymore.

##### Compatible with versions Node.js >=16.10
First, make sure you have Node v.18.12.1 installed.
(If you are not sure what version of node you have, you can use the command ``` node -v ``` to see your current version of node)

Now, inside the main directory, where the package.json file is located, use the followings commands to install Yarn using Corepack:
``` bash
corepack enable
corepack prepare yarn@1.22.19 --activate
```
``` bash
yarn
yarn install
```

With this done, the app will be ready for use!

## Usage

To run the app in development mode, use:
``` bash
yarn dev
```
And finally, access http://localhost:3000/ in your browser to interact with the project

(In the project you have a folder called uploads located in public/assets/ in which you can find profile and publication images to be able to use.)

## License
This project can be used freely and can be continued for the development of a business vision project:
- Perhaps to use web3 with an implementation of some tokenized functionality to allow users to reward their favorite content creators.
- Or some other type of use case that can be useful to create a profitable business.

## Project status

Done
